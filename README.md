# RTS Camera Controller for Unity

RTS Camera Controller is a component which gives a camera RTS style behaviour.

The RTSCameraController component relies on Unity's Input System package for user input so make sure that you have that downloaded and enabled.

## How To Use:
Add the RTSCameraController component and Input Systems Player Input component to a Camera.  Then assign a GameObject for the camera to focus on by using the component window.

Default bindings are as follows:
- Double left click to change camera focus
- Scroll to zoom in/out
- Middle mouse hold to orbit the cameras focus
- Space Button recenters focus on targetGameObject
- WASD moves freely around

## How It Works:
The RTS Camera Controller use an Arcball style set of functions.  What this means is that the Camera's position lies on a sphere.  

The center of the sphere is called the focusPoint.  This is assigned in the component window and gives the camera something to focus on when run.  Once started, you can change the focus by using the Select Target key on any object with a collider that detects raycast hits.  Using the Movement keys allows free movement along the X and Z axis.  Pressing the Center On Target button will recenter the camera on it's current object focus.

The camera functions as though it is fixed to the surface of a sphere. A point on a sphere's surface can be represented by the spheres radius, phi, and theta.  radius is the distance from the spheres surface to the center of the sphere.  Theta is the horizontal angle which it lies on, and Phi is the vertical angle.  With these three properties we can determine the camera's position on the sphere and convert this to the cartesian coordinates used by Unity.

## Implemented Features:
- Increase/Decrease the radius of the sphere
- Set the camera's position on the sphere
- Set the camera's focus
- Navigate freely
- refocus on camera's focus after losing focus


## Planned Features:
- Smooth camera transitions for moving, scrolling, and recentering.
