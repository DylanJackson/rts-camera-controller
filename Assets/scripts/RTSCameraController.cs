using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Camera))]

public class RTSCameraController : MonoBehaviour
{
    private RtsCameraActions _inputActions;
    private Camera thisCamera;


    public GameObject targetGameobject;


    public float radius = 5f;
    public float rotationSensitivity = 1f;
    public float scrollSensitivity = 1f;
    public float moveSensitivity = 1f;

    // The mouse cursor's position during the last frame
    Vector3 last = new Vector3();
    // The spherical coordinates
    Vector3 sc = new Vector3();
    //Offset from target
    Vector3 focusPoint = new Vector3();

    Keyboard keyboard;
    Mouse mouse;

    bool isDragging = false;

    void Start()
    {
        this.transform.position = new Vector3(radius, radius, 0.0f);
        focusPoint = targetGameobject.transform.position;
        this.transform.LookAt(targetGameobject.transform.position);
        sc = getSphericalCoordinates(this.transform.position);

        thisCamera = GetComponent<Camera>();

        keyboard = Keyboard.current;
        mouse = Mouse.current;
    }

    // Update is called once per frame
    void Update()
    {
        //This controls camera dragging
        if (_inputActions.RTSCamera.Drag.triggered) { isDragging = true; last = mouse.position.ReadValue();}
        if (isDragging) { UpdateSphereAngles(); }
        _inputActions.RTSCamera.Drag.canceled += _ => isDragging = false;

        //This controls movement
        if (_inputActions.RTSCamera.Movement.ReadValue<Vector2>() != Vector2.zero) {UpdateSpherePosition(_inputActions.RTSCamera.Movement.ReadValue<Vector2>());}

        //This controls zoom
        if( _inputActions.RTSCamera.Scroll.ReadValue<Vector2>() != Vector2.zero) { UpdateSphereRadius(_inputActions.RTSCamera.Scroll.ReadValue<Vector2>()); }

        //This controls centering on target
        _inputActions.RTSCamera.CenterOnTarget.performed += _ => CenterOnTarget();

        //This controls Selecting a new target
        _inputActions.RTSCamera.SelectTarget.performed += _ => SelectNewTarget();


    }

    void CenterOnTarget()
    {
        focusPoint = targetGameobject.transform.position;

        // Calculate the cartesian coordinates for unity
        transform.position = getCartesianCoordinates(sc) + focusPoint;

        // Make the camera look at the target
        transform.LookAt(focusPoint);
    }

    void SelectNewTarget()
    {
        RaycastHit hit;
        Ray ray = thisCamera.ScreenPointToRay(new Vector3(mouse.position.x.ReadValue(),mouse.position.y.ReadValue(),0));

        if (Physics.Raycast(ray, out hit))
        {
            targetGameobject = hit.transform.gameObject;
        }

        CenterOnTarget();
    }

    void UpdateSphereRadius(Vector2 input)
    {
        sc.x += -input.y;

        if(sc.x < 1f) { sc.x = 1f; }

        // Calculate the cartesian coordinates for unity
        transform.position = getCartesianCoordinates(sc) + focusPoint;

        // Make the camera look at the target
        transform.LookAt(focusPoint);
    }

    void UpdateSphereAngles()
    {
        // Get the deltas that describe how much the mouse cursor got moved between frames
        float dx = (last.x - mouse.position.x.ReadValue()) * rotationSensitivity;
        float dy = (last.y - mouse.position.y.ReadValue()) * rotationSensitivity;

        // Only update the camera's position if the mouse got moved in either direction
        if (dx != 0f || dy != 0f)
        {
            // Rotate the camera left and right
            sc.y += dx * Time.deltaTime;

            // Rotate the camera up and down
            // Prevent the camera from turning upside down (1.5f = approx. Pi / 2)
            sc.z = Mathf.Clamp(sc.z + dy * Time.deltaTime, -1.5f, 1.5f);

            // Calculate the cartesian coordinates for unity
            transform.position = getCartesianCoordinates(sc) + focusPoint;

            // Make the camera look at the target
            transform.LookAt(focusPoint);
        }

        // Update the last mouse position
        last = mouse.position.ReadValue();
    }

    public void UpdateSpherePosition(Vector2 inputMovement)
    {
        Vector3 previousPosition = transform.position;

        Vector3 finalVector = new Vector3(inputMovement.x * Time.deltaTime  , 0f, inputMovement.y * Time.deltaTime).normalized;

        finalVector = transform.TransformDirection(finalVector);
        finalVector.y = 0;

        transform.position += finalVector.normalized;
        focusPoint += (transform.position - previousPosition);
    }








    /// <summary>
    /// Given Cartesian coordinates (Vector3)  Translates it into a point on a the face of a sphere.
    /// </summary>
    /// <param name="cartesian"></param>
    /// <returns></returns>
    Vector3 getSphericalCoordinates(Vector3 cartesian)
    {
        float r = Mathf.Sqrt(
            Mathf.Pow(cartesian.x, 2) +
            Mathf.Pow(cartesian.y, 2) +
            Mathf.Pow(cartesian.z, 2)
        );

        float phi = Mathf.Atan2(cartesian.z / cartesian.x, cartesian.x);
        float theta = Mathf.Acos(cartesian.y / r);

        if (cartesian.x < 0)
            phi += Mathf.PI;

        return new Vector3(r, phi, theta);
    }
    /// <summary>
    /// Given spherical coordinates, returns it's position in 3d space as cartesian coordinates
    /// </summary>
    /// <param name="spherical"></param>
    /// <returns></returns>
    Vector3 getCartesianCoordinates(Vector3 spherical)
    {
        Vector3 ret = new Vector3();

        ret.x = spherical.x * Mathf.Cos(spherical.z) * Mathf.Cos(spherical.y);
        ret.y = spherical.x * Mathf.Sin(spherical.z);
        ret.z = spherical.x * Mathf.Cos(spherical.z) * Mathf.Sin(spherical.y);

        return ret;
    }
    private void Awake()
    {
        Debug.Log("awake");
        _inputActions = new RtsCameraActions();
    }
    private void OnEnable()
    {
        Debug.Log("OnEnable");

        _inputActions.RTSCamera.Enable();
    }
    private void OnDisable()
    {
        Debug.Log("OnDisable");
        _inputActions.RTSCamera.Disable();
    }
}

/*
    Space bar should recenter on target

    Implement sensitivity
     */