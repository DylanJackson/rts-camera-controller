// GENERATED AUTOMATICALLY FROM 'Assets/InputSystemStuff/RtsCameraActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @RtsCameraActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @RtsCameraActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""RtsCameraActions"",
    ""maps"": [
        {
            ""name"": ""RTS Camera"",
            ""id"": ""70061afd-1151-4884-bd1c-c92f72b6a536"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Value"",
                    ""id"": ""242b1496-dc40-49be-94b4-91acbd110383"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Scroll"",
                    ""type"": ""Value"",
                    ""id"": ""ab365999-ea5d-4f0a-88a5-712110f3f2c1"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Drag"",
                    ""type"": ""Value"",
                    ""id"": ""9eadef3d-fcfe-42bc-82df-7a42ca69a55c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Center On Target"",
                    ""type"": ""Button"",
                    ""id"": ""7782c59c-49bf-427a-8a0c-6af8315c115b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Select Target"",
                    ""type"": ""Button"",
                    ""id"": ""fe757e96-e9bf-4724-95dd-cb4c5220c33b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""MultiTap""
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""696f6538-a2cb-4de2-89c1-5d2ac431619e"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""6c18840d-5bc8-47c2-a388-427011fd01af"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""RTSCameraControlScheme;Keyboard&Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""54731cf0-7a74-4869-be67-10b5bd3f243d"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""RTSCameraControlScheme;Keyboard&Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""c186eeac-b151-4c48-a63e-42c21bb83e47"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""RTSCameraControlScheme;Keyboard&Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""b75bc4c1-43b3-4ad5-990b-598c7e910ed0"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""RTSCameraControlScheme;Keyboard&Mouse"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""dfd4b4a8-3349-4890-966b-dc47bb351bc6"",
                    ""path"": ""<Mouse>/scroll"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""RTSCameraControlScheme"",
                    ""action"": ""Scroll"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""81231f98-79ab-4b2d-8238-5e9e96117ead"",
                    ""path"": ""<Mouse>/middleButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""RTSCameraControlScheme"",
                    ""action"": ""Drag"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8e84af21-4493-4467-844d-2a5535b29fb8"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": ""RTSCameraControlScheme"",
                    ""action"": ""Center On Target"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f3e30e44-c50b-4656-b211-dda70a8eab78"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""RTSCameraControlScheme"",
                    ""action"": ""Select Target"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard&Mouse"",
            ""bindingGroup"": ""Keyboard&Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Touch"",
            ""bindingGroup"": ""Touch"",
            ""devices"": [
                {
                    ""devicePath"": ""<Touchscreen>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Joystick"",
            ""bindingGroup"": ""Joystick"",
            ""devices"": [
                {
                    ""devicePath"": ""<Joystick>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""XR"",
            ""bindingGroup"": ""XR"",
            ""devices"": [
                {
                    ""devicePath"": ""<XRController>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""RTSCameraControlScheme"",
            ""bindingGroup"": ""RTSCameraControlScheme"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // RTS Camera
        m_RTSCamera = asset.FindActionMap("RTS Camera", throwIfNotFound: true);
        m_RTSCamera_Movement = m_RTSCamera.FindAction("Movement", throwIfNotFound: true);
        m_RTSCamera_Scroll = m_RTSCamera.FindAction("Scroll", throwIfNotFound: true);
        m_RTSCamera_Drag = m_RTSCamera.FindAction("Drag", throwIfNotFound: true);
        m_RTSCamera_CenterOnTarget = m_RTSCamera.FindAction("Center On Target", throwIfNotFound: true);
        m_RTSCamera_SelectTarget = m_RTSCamera.FindAction("Select Target", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // RTS Camera
    private readonly InputActionMap m_RTSCamera;
    private IRTSCameraActions m_RTSCameraActionsCallbackInterface;
    private readonly InputAction m_RTSCamera_Movement;
    private readonly InputAction m_RTSCamera_Scroll;
    private readonly InputAction m_RTSCamera_Drag;
    private readonly InputAction m_RTSCamera_CenterOnTarget;
    private readonly InputAction m_RTSCamera_SelectTarget;
    public struct RTSCameraActions
    {
        private @RtsCameraActions m_Wrapper;
        public RTSCameraActions(@RtsCameraActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_RTSCamera_Movement;
        public InputAction @Scroll => m_Wrapper.m_RTSCamera_Scroll;
        public InputAction @Drag => m_Wrapper.m_RTSCamera_Drag;
        public InputAction @CenterOnTarget => m_Wrapper.m_RTSCamera_CenterOnTarget;
        public InputAction @SelectTarget => m_Wrapper.m_RTSCamera_SelectTarget;
        public InputActionMap Get() { return m_Wrapper.m_RTSCamera; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(RTSCameraActions set) { return set.Get(); }
        public void SetCallbacks(IRTSCameraActions instance)
        {
            if (m_Wrapper.m_RTSCameraActionsCallbackInterface != null)
            {
                @Movement.started -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnMovement;
                @Movement.performed -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnMovement;
                @Movement.canceled -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnMovement;
                @Scroll.started -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnScroll;
                @Scroll.performed -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnScroll;
                @Scroll.canceled -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnScroll;
                @Drag.started -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnDrag;
                @Drag.performed -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnDrag;
                @Drag.canceled -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnDrag;
                @CenterOnTarget.started -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnCenterOnTarget;
                @CenterOnTarget.performed -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnCenterOnTarget;
                @CenterOnTarget.canceled -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnCenterOnTarget;
                @SelectTarget.started -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnSelectTarget;
                @SelectTarget.performed -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnSelectTarget;
                @SelectTarget.canceled -= m_Wrapper.m_RTSCameraActionsCallbackInterface.OnSelectTarget;
            }
            m_Wrapper.m_RTSCameraActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Movement.started += instance.OnMovement;
                @Movement.performed += instance.OnMovement;
                @Movement.canceled += instance.OnMovement;
                @Scroll.started += instance.OnScroll;
                @Scroll.performed += instance.OnScroll;
                @Scroll.canceled += instance.OnScroll;
                @Drag.started += instance.OnDrag;
                @Drag.performed += instance.OnDrag;
                @Drag.canceled += instance.OnDrag;
                @CenterOnTarget.started += instance.OnCenterOnTarget;
                @CenterOnTarget.performed += instance.OnCenterOnTarget;
                @CenterOnTarget.canceled += instance.OnCenterOnTarget;
                @SelectTarget.started += instance.OnSelectTarget;
                @SelectTarget.performed += instance.OnSelectTarget;
                @SelectTarget.canceled += instance.OnSelectTarget;
            }
        }
    }
    public RTSCameraActions @RTSCamera => new RTSCameraActions(this);
    private int m_KeyboardMouseSchemeIndex = -1;
    public InputControlScheme KeyboardMouseScheme
    {
        get
        {
            if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard&Mouse");
            return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
        }
    }
    private int m_GamepadSchemeIndex = -1;
    public InputControlScheme GamepadScheme
    {
        get
        {
            if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
            return asset.controlSchemes[m_GamepadSchemeIndex];
        }
    }
    private int m_TouchSchemeIndex = -1;
    public InputControlScheme TouchScheme
    {
        get
        {
            if (m_TouchSchemeIndex == -1) m_TouchSchemeIndex = asset.FindControlSchemeIndex("Touch");
            return asset.controlSchemes[m_TouchSchemeIndex];
        }
    }
    private int m_JoystickSchemeIndex = -1;
    public InputControlScheme JoystickScheme
    {
        get
        {
            if (m_JoystickSchemeIndex == -1) m_JoystickSchemeIndex = asset.FindControlSchemeIndex("Joystick");
            return asset.controlSchemes[m_JoystickSchemeIndex];
        }
    }
    private int m_XRSchemeIndex = -1;
    public InputControlScheme XRScheme
    {
        get
        {
            if (m_XRSchemeIndex == -1) m_XRSchemeIndex = asset.FindControlSchemeIndex("XR");
            return asset.controlSchemes[m_XRSchemeIndex];
        }
    }
    private int m_RTSCameraControlSchemeSchemeIndex = -1;
    public InputControlScheme RTSCameraControlSchemeScheme
    {
        get
        {
            if (m_RTSCameraControlSchemeSchemeIndex == -1) m_RTSCameraControlSchemeSchemeIndex = asset.FindControlSchemeIndex("RTSCameraControlScheme");
            return asset.controlSchemes[m_RTSCameraControlSchemeSchemeIndex];
        }
    }
    public interface IRTSCameraActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnScroll(InputAction.CallbackContext context);
        void OnDrag(InputAction.CallbackContext context);
        void OnCenterOnTarget(InputAction.CallbackContext context);
        void OnSelectTarget(InputAction.CallbackContext context);
    }
}
